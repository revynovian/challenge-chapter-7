# Chapter 7 - Challenge

## Design Pattern and Authentication

---

### How to run project

>STEP 1 : install dependencies

`npm run install`

>STEP 2 : make environment variables file (.env) based on .env.example

eg:
| Environtment variables
| :--------------  
|DB_USER=dbuser  
|DB_NAME=dbname
|DB_PASSWORD=dbpass
|DB_HOST=localhost

>STEP 3 : create database

`npm run dbcreate`

>STEP 4 : migrate database schema

`npm run dbmigrate`

>STEP 5 : populate database using seeder

`npm run dbseed`

>STEP 6 : run the app and server will be ready at port 3000

`npm run dev`

---

### Available Endpoints

|    Endpoint      |  Description | Protected | Auth Strategy |
| :--------------  | :------------------- |:----------|:-- |
|        /         |  Landing Page        | no  | - |
|      /game       |  Game                | no  | - |
|  /admin/login    |  admin login page    | no  | - |
|  /admin/signup   |  admin signup page   | no  | - |
|  /admin/dashboard|  admin dashboard     | yes | local |
|  /admin/users    |  user profile page   | yes | local |
|  /admin/adduser |  add user data   | yes | local |

usersAPI
|    Endpoint      |  Description | Protected | Auth Strategy |
| :--------------  | :------------------- |:----------|:-- |
|  /api/users/register |  user registration page   | no | - |
|  /api/users/login |  user login page   | no | - |
|  /api/users/room-status |  room list   | yes | JWT |
|  /api/users/create-room |  create room   | yes | JWT |
|  /api/users/join-room |  join room   | yes | JWT |
|  /api/users/fight |  post player's pick   | yes | JWT |
|  /api/users/result |  get specific match result  | yes | JWT |
