module.exports = {
  // if user is not login redirect to login page
  isAuthenticated : (req, res, next) => {
    if(req.isAuthenticated()) return next()
    res.redirect('/admin/login')
  },
  // if user is already login redirect to dashboard
  isNotAuthenticated : (req,res, next) => {
    if(req.isAuthenticated()) return res.redirect('/admin/dashboard')
    next()
  }
}