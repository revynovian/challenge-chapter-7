const passportJwt = require("../config/passport-jwt-config");

module.exports = passportJwt.authenticate('jwt', {
  session: false
})