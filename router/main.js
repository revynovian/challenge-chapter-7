const router = require('express').Router();

// main routes
router.get('/', (req, res) => {
  res.render('index', { title: 'chapter-3' });
});
router.get('/game', (req, res) => {
  res.render('game', { title: 'chapter-4' });
});

module.exports = router;