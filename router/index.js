const router = require('express').Router();
const main = require('./main')
const admin = require('./admin')
const usersApi = require('./users-api')

router.use("/", main)
router.use("/admin", admin)
router.use("/api/users", usersApi)

module.exports = router;