const express = require('express');
const router = express.Router();
const { dashboardPage, usersPage, registerPage, createUser, deleteUser, updateUser} = require('../controllers/admin-controllers');
const { loginPage, loginCheck, signupPage, createAdmin, logout } = require('../controllers/admin-auth-controllers')
const { isAuthenticated, isNotAuthenticated } = require('../middleware/passport-restrict')

// admin login and signup
router.get('/login', isNotAuthenticated,loginPage);
router.post('/login', loginCheck);
router.get('/signup', isNotAuthenticated,signupPage);
router.post('/signup', createAdmin);
router.get('/logout', logout)

// admin dashboard
router.get('/dashboard', isAuthenticated, dashboardPage);
router.get('/adduser',isAuthenticated, registerPage);
router.post('/adduser', createUser);
router.get('/users', usersPage);
router.get('/delete', deleteUser);
router.post('/update', updateUser);

module.exports = router;
