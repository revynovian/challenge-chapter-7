const express = require('express');
const router = express.Router();
const { register, login, test } = require('../controllers/api-auth-controllers');
const { createRoom, joinRoom, playerFight, getResult, roomStatus } = require('../controllers/api-controllers')
const isJwtVerified = require('../middleware/passport-jwt-restrict');

//register and login
router.post('/register', register);
router.post('/login', login);

// multiplayer routes
router.get('/room-status',isJwtVerified, roomStatus);
router.post('/create-room',isJwtVerified, createRoom);
router.post('/join-room', isJwtVerified, joinRoom);
router.post('/fight', isJwtVerified, playerFight);
router.get('/result', isJwtVerified ,getResult)

module.exports = router;
