'use strict';
const { Model } = require('sequelize');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
  class UserGame extends Model {
    static associate({ UserGameBio, UserGameHistory }) {
      this.hasOne(UserGameBio, { foreignKey: 'user_id'});
      this.hasMany(UserGameHistory, { foreignKey: 'user_id' });
    }
    /* login authentication using JWT strategy */
    // comparing hash
    checkedPassword = password => bcrypt.compareSync(password, this.password)
    // generate token
    generateToken = () => {
      const payload = {
        id: this.user_id,
        username: this.username
      }
      console.log(payload)
      // by default jwt using HS256 algorithm using secret key to verivy token's signature
      const secretKey = "rahasia"
      const token = jwt.sign(payload, secretKey)
      return token
    }
    // authenticate player
    static authenticateUser = async ({username, password}) => {
      try {
        const user = await this.findOne({where: {username}})
        if(!user) return Promise.reject("user not found!");
        
        const isPasswordValid = user.checkedPassword(password)
        if(!isPasswordValid) return Promise.reject("password incorect")

        return Promise.resolve(user)
      }
      catch (e){
        return Promise.reject(e)
      }
    }
  }
  UserGame.init(
    {
      user_id: {
        primaryKey: true,
        type: DataTypes.UUID,
        allowNull: false,
      },
      username: {
        type: DataTypes.STRING,
        allowNull: false
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false
      },
      rank: {
        type: DataTypes.STRING,
        defaultValue: 'Bronze',
      },
      game_point: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
    },
    {
      sequelize,
      modelName: 'UserGame',
      tableName: 'user_game',
      underscored: true,
    }
  );
  return UserGame;
};
