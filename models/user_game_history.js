'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class UserGameHistory extends Model {
    static associate({ UserGame }) {
      this.belongsTo(UserGame, { foreignKey: 'user_id'});
    }
  }
  UserGameHistory.init(
    {
      game_instance: {
        primaryKey: true,
        type: DataTypes.UUID,
        allowNull: false,
      },
      match_point: DataTypes.INTEGER,
      user_id: {
        type: DataTypes.UUID,
        references: {
          model: 'user_game',
          key: 'user_id',
        },
      },
    },
    {
      sequelize,
      modelName: 'UserGameHistory',
      tableName: 'user_game_history',
      underscored: true,
    }
  );
  return UserGameHistory;
};
