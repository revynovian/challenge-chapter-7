'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class multiplayerRoom extends Model {
    
  }
  multiplayerRoom.init(
    {
      room_id: {
        primaryKey: true,
        type: DataTypes.UUID,
        allowNull: false,
      },
      player1_id: {
        type: DataTypes.UUID,
      },
      player2_id: {
        type: DataTypes.UUID,
      },
      player1_pick: {
        type: DataTypes.ARRAY(DataTypes.STRING)
      },
      player2_pick: {
        type: DataTypes.ARRAY(DataTypes.STRING)
      },
      result :{
        type: DataTypes.STRING
      }
    },
    {
      sequelize,
      modelName: 'multiplayerRoom',
      tableName: 'multiplayer_room',
      underscored: true,
      updatedAt: false,
      createdAt: false
    }
  );
  return multiplayerRoom;
};
