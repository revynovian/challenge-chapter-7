'use strict';
const { Model } = require('sequelize');
const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
  class SuperAdmin extends Model {
    static #hashing = (password) => bcrypt.hashSync(password, 10)

    static register = ({username, password}) => {
      const hashedPassword = this.#hashing(password)
      return this.create ({username, password : hashedPassword})
    }

  }
  SuperAdmin.init(
    {
      admin_id: {
        primaryKey: true,
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
      },
      username: {
        type: DataTypes.STRING,
        unique: true
      },
      password: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'SuperAdmin',
      tableName: 'super_admin',
      underscored: true,
    }
  );
  return SuperAdmin;
};
