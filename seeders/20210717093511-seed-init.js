'use strict';
const faker = require('faker');
const bcrypt = require('bcrypt');

module.exports = {
  up: async (queryInterface, Sequelize) => {

    /* USER_GAME SEEDER*/
    for (let i = 1; i < 16; i++) {
      
      // create random rank based on GamePoint
      const gamePoint = Math.floor(Math.random() * 1500);
      let rank;
      if (gamePoint <= 500) rank = 'Bronze';
      else if (gamePoint <= 1000) rank = 'Silver';
      else rank = 'Gold';

      // generate userid
      let fake_id = faker.datatype.uuid();

      // seeder user_game
      await queryInterface.bulkInsert('user_game', [
        {
          user_id: fake_id,
          username: faker.internet.userName(),
          password: faker.internet.password(),
          game_point: gamePoint,
          rank: rank,
          created_at: new Date(),
          updated_at: new Date(),
        },
      ]);

      // seeder user_game_biodata
      await queryInterface.bulkInsert('user_game_bio', [
        {
          id: faker.datatype.uuid(),
          first_name: faker.name.firstName(),
          last_name: faker.name.lastName(),
          email: faker.internet.email(),
          country: faker.address.countryCode(),
          user_id: fake_id,
          created_at: new Date(),
          updated_at: new Date(),
        },
      ]);

      // create random data , how many times user playing (1-5)
      const matchPlayed = Math.floor(Math.random() * 5);

      // seeder user_game_history
      for (let p = 0; p <= matchPlayed; p++) {
        await queryInterface.bulkInsert('user_game_history', [
          {
            game_instance: faker.datatype.uuid(),
            match_point: Math.floor(Math.random() * 150 + 10),
            user_id: fake_id,
            created_at: new Date(),
            updated_at: new Date(),
          },
        ]);
      }
    }

    /* MULTIPLAYER_ROOM SEEDER*/
    const player1_id = faker.datatype.uuid();
    const player2_id = faker.datatype.uuid();
    const player1_pick = ["R","P","S","P","R"];
    const player2_pick = ["P","P","R","P","S"];
    const result = "LOSE,DRAW,LOSE,DRAW,WIN"
    
    // create player1 and player2 username and password
    let hashedPassword = bcrypt.hashSync(faker.internet.password(),10)
    await queryInterface.bulkInsert('user_game', [{
      user_id: player1_id,
      username: faker.internet.userName(),
      password: hashedPassword,
      created_at: new Date(),
      updated_at: new Date()
    }])
    await queryInterface.bulkInsert('user_game', [{
      user_id: player2_id,
      username: faker.internet.userName(),
      password: hashedPassword,
      created_at: new Date(),
      updated_at: new Date()
    }])

    // create 5 row with unique data
    // room1 , only player1 joined
    await queryInterface.bulkInsert('multiplayer_room', [{
      room_id: faker.datatype.uuid(),
      player1_id
    }])
    // room2 , player1 and player 2 joined but not pick the card yet
    await queryInterface.bulkInsert('multiplayer_room', [{
      room_id: faker.datatype.uuid(),
      player1_id,
      player2_id
    }])
    // room3 , player1 picks and player 2 not join yet
    await queryInterface.bulkInsert('multiplayer_room', [{
      room_id: faker.datatype.uuid(),
      player1_id,
      player1_pick
    }])
    // room4 , player1 and player 2 pick the card but the result still pending
    await queryInterface.bulkInsert('multiplayer_room', [{
      room_id: faker.datatype.uuid(),
      player1_id,
      player2_id,
      player1_pick,
      player2_pick
    }])
    // room5 , match finished , players already pick and get the result
    await queryInterface.bulkInsert('multiplayer_room', [{
      room_id: faker.datatype.uuid(),
      player1_id,
      player2_id,
      player1_pick,
      player2_pick,
      result
    }])

  },

  down: async (queryInterface, Sequelize) => {
    // deleting parent table will delete other table's record, with cascade on delete
    await queryInterface.bulkDelete('user_game', null, {});
    await queryInterface.bulkDelete('multiplayer_room', null, {});
  },
};
