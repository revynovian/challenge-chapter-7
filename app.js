const express = require('express');
const app = express();

require('dotenv').config();
const PORT = 3000 || process.env.PORT;
const { sequelize } = require('./models');
const session = require('express-session');
const flash = require('express-flash')

// express middlware
app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(express.urlencoded({extended: false}));
app.use(express.json())

app.use(session({
  secret: 'secret',
  resave: false,
  saveUninitialized: false
}))

// passportJs
const passport = require('./config/passport-config')
const passportJwt = require('./config/passport-jwt-config')
app.use(passportJwt.initialize())
app.use(passport.initialize())
app.use(passport.session()) 
app.use(flash())

// routes
const router = require('./router');
app.use(router);


// server error handler 5xx
app.use((err, req, res, next) => {
  res.status(500).render('errors', { title: 'error page', status: '500' });
  next();
});
// client error handler 4xx
app.use((req, res, next) => {
  res.status(404).render('errors', { title: 'error page', status: '404' });
});

// check connection to database
sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch((err) => {
    console.error('Unable to connect to the database:', err);
  });

// express server connection
app.listen(PORT, () => console.log(`Server running at http://localhost:${PORT}`));
