const { SuperAdmin } = require('../models');
const passport = require('../config/passport-config')

module.exports = {
  loginPage : (req, res) => {
    res.render('login', { title: 'login',message: req.flash('error') });
  },
  loginCheck : passport.authenticate('local',{
    successRedirect: '/admin/dashboard',
    failureRedirect: '/admin/login',
    failureFlash: true
  })
  ,
  signupPage : (req, res) => {
    res.render('signup', { title: 'signup' });
  }, 
  createAdmin : async (req, res) => {
    try {
      await SuperAdmin.register(req.body)
      res.redirect('/admin/login')
    }
    catch {
      res.redirect('/admin/register')
    }
  },
  logout : (req, res) => {
    req.logOut()
    res.redirect('/admin/login')
  }
}