const {UserGame, UserGameBio} = require ('../models')
const { v4: uuidv4 } = require('uuid');
const bcrypt = require('bcrypt');

function format (user) {
  const {user_id, username} = user
  return {
    user_id,
    username,
    accessToken : user.generateToken()
  }
}

module.exports = {
  register: async (req, res) => {
    try {
      const user_id = uuidv4();
      const hashedPassword = bcrypt.hashSync(req.body.password,10)
      await UserGame.create({
        // required information
        user_id,
        username: req.body.username,
        password: hashedPassword,
      });
      // create biodata entry
      await UserGameBio.create({
        id: uuidv4(),
        // foreign key
        user_id,
        // optional information
        first_name:req.body.firstName,
        last_name:req.body.lastName,
        email:req.body.email,
        country:req.body.country
      })
      res.status(200).json({"status": "registration success"})
    }
    catch {
      res.status(400).json({
        status: 'error',
        error: 'req body cannot be empty',
      });
    }
  },
  login: (req,res) => {
    UserGame.authenticateUser(req.body)
    .then((user) => {
      res.json(format(user))
    }).catch((err) => {
      res.status(400).json({
        status: 'bad request',
        error: err,
      });
    });
  }
}