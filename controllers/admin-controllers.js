const { UserGame, UserGameBio, UserGameHistory } = require('../models');

// === DASHBOARD ====
// get all user
async function dashboardPage(req, res) {
  try {
    const userGame = await UserGame.findAll();
    const userLead = await UserGame.findAll({
      attributes: ['username', 'game_point'],
      order: [['game_point', 'DESC']],
      limit: 5,
      include: [
        {
          model: UserGameBio,
          attributes: ['country'],
        },
      ],
    });
    res.render('dashboard', { title: 'Dashboard', userLead, userGame , user: req.user.dataValues});
  } catch (err) {
    console.log(err);
  }
}

// get one user biodata
async function usersPage(req, res) {
  try {
    const userProfile = await UserGame.findAll({
      attributes: ['username', 'game_point', 'rank', 'user_id'],
      include: [
        { model: UserGameBio, attributes: ['first_name', 'last_name', 'email', 'country','user_id'] },
        { model: UserGameHistory, attributes: ['match_point', 'created_at'] },
      ],
      where: {
        user_id: req.query.id,
      },
    });

    const userRank = await UserGame.findAll({
      attributes: ['user_id'],
      order: [['game_point', 'DESC']],
    });

    // ger user global rank based on gamepoint
    const userList = []
    userRank.forEach(e => userList.push(e['user_id']));
    const userGetRank = userList.indexOf(req.query.id)+1

    res.render('userProfile', { title: 'Profile', userProfile, userGetRank, user: req.user.dataValues });
  } catch (err) {
    console.log(err);
  }
}

// add new user
function registerPage(req, res) {
  res.render('register', { title: 'Register',  user: req.user.dataValues });
}

const { v4: uuidv4 } = require('uuid');

async function createUser(req, res) {
  const user_id = uuidv4();
  try {
    // create username and pass
    await UserGame.create({
      user_id,
      username: req.body.username,
      password: req.body.password,
    });
    // also create biodata entry 
    await UserGameBio.create({
      id: uuidv4(),
      user_id,
      first_name:req.body.firstName,
      last_name:req.body.lastName,
      email:req.body.email,
      country:req.body.country
    })

    res.redirect('/admin/dashboard');
  } catch (err) {
    console.log(err);
  }
}

// delete user
async function deleteUser(req, res) {
  try {
    await UserGame.destroy({
      where: {
        user_id: req.query.id,
      },
    });
    res.redirect('/admin/dashboard');
  } catch (err) {
    console.log(err);
  }
}

// update user
async function updateUser(req, res) {
  try {
    await UserGameBio.update(
      {
        first_name: req.body.firstName,
        last_name: req.body.lastName,
        email: req.body.email,
        country: req.body.country,
      },
      { where: { user_id: req.query.id } }
    );
    res.redirect('/admin/dashboard');
  } catch (err) {
    console.log(err);
  }
}

module.exports = { dashboardPage, usersPage, registerPage, createUser, deleteUser, updateUser };
