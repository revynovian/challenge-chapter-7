const { multiplayerRoom } = require('../models');
const { v4: uuidv4 } = require('uuid');

// game's logic
const PICKS = [
  {
    choice: 'R',
    beats: 'S',
    losesTo: 'P',
  },
  {
    choice: 'P',
    beats: 'R',
    losesTo: 'S',
  },
  {
    choice: 'S',
    beats: 'P',
    losesTo: 'R',
  },
];

function getResult(pick1, pick2) {
  for (item in PICKS) {
    if (pick1 == PICKS[item].choice) {
      if (PICKS[item].beats.includes(pick2)) {
        return 'WIN';
      } else if (PICKS[item].losesTo.includes(pick2)) {
        return 'LOSE';
      } else {
        return 'DRAW';
      }
    }
  }
}
// contoh request body di fight room
const example = {
  room_id: '7d1dcb33-1068-4051-b869-04407d025c69',
  picks: ['R', 'S', 'S', 'S', 'S'],
};

// multiplayer controller
module.exports = {
  roomStatus: async (req, res) => {
    try {
      const allRoom = await multiplayerRoom.findAll();
      const listRoom = [];
      for (room in allRoom) {
        let getRoom = allRoom[room].room_id;
        let getResult = allRoom[room].result;
        let getPlayer2 = allRoom[room].player2_id;
        let getPlayer1_pick = allRoom[room].player1_pick;
        let getPlayer2_pick = allRoom[room].player2_pick;

        // check room status
        let status = 'pending';
        let p1_pickStatus = 'already picked';
        let p2_pickStatus = 'already picked';

        if (getResult) {
          status = 'match finished';
        }
        if (!getPlayer2) {
          status = 'room available';
        }
        if (!getPlayer1_pick) {
          p1_pickStatus = 'player1 not pick yet';
        }
        if (!getPlayer2_pick) {
          p2_pickStatus = 'player2 not pick yet';
        }

        let allRoomStatus = { room_id: getRoom, status, p1_pickStatus, p2_pickStatus };
        listRoom.push(allRoomStatus);
      }
      res.json({ listRoom });
    } catch (err) {
      res.json({ error: 'failed to get room status' });
    }
  },
  createRoom: async (req, res) => {
    try {
      const room_id = uuidv4();
      const { user_id } = req.user;
      const roomInfo = await multiplayerRoom.create({
        room_id,
        player1_id: user_id,
      });
      res.json({ roomInfo });
    } catch (err) {
      res.json({ error: 'failed to create room' });
    }
  },
  joinRoom: async (req, res) => {
    try {
      const { user_id } = req.user;
      const findRoom = await multiplayerRoom.findOne({
        where: {
          room_id: req.body.room_id,
        },
      });
      if (!findRoom) res.json({ status: "can't find the room" });
      else if (findRoom.player2_id) res.json({ status: 'room is full' });
      else {
        await multiplayerRoom.update({ player2_id: user_id }, { where: { room_id: req.body.room_id } });
        res.json({ status: 'you have joined the room', 'how to play': 'go to the fight room with room_id and pick 5 card', example });
      }
    } catch (err) {
      res.json({ status: 'failed to join room' });
    }
  },
  playerFight: async (req, res) => {
    try {
      // check user id
      const { user_id } = req.user;
      // check the player_id in the room
      const player1 = await multiplayerRoom.findOne({
        where: {
          player1_id: user_id,
          room_id: req.body.room_id,
        },
      });
      const player2 = await multiplayerRoom.findOne({
        where: {
          player2_id: user_id,
          room_id: req.body.room_id,
        },
      });
      if (!(player1 || player2)) {
        res.json({ status: 'this room is full' });
        return;
      }
      // validate user input
      // cards pick must be 5
      if (req.body.picks.length != 5) {
        res.json({ status: 'your picks must be 5 cards' });
        return;
      }
      // check player already pick or not
      // if not pick yet push the picks to database
      if (player1 && !player1.player1_pick) {
        await multiplayerRoom.update({ player1_pick: req.body.picks }, { where: { room_id: req.body.room_id } });
        res.json({ status: 'ready', player_number: '1', 'your picks': req.body.picks });
      } else if (player2 && !player2.player2_pick) {
        await multiplayerRoom.update({ player2_pick: req.body.picks }, { where: { room_id: req.body.room_id } });
        res.json({ status: 'ready', player_number: '2', 'your picks': req.body.picks });
      } else {
        res.json({ status: "you've already pick" });
      }
    } catch (err) {
      res.json({ error: 'failed to pick the card, check your request body' });
    }
  },
  // get result of specific room_id
  getResult: async (req, res) => {
    try {
      const findRoom = await multiplayerRoom.findOne({
        where: {
          room_id: req.body.room_id,
        },
      });
      if (!findRoom) res.json({ status: "can't find the room" });
      else {
        const player1_pick = findRoom.player1_pick;
        const player2_pick = findRoom.player2_pick;
        // calculate result
        const result = [];
        for (let i = 0; i < player1_pick.length; i++) {
          result.push(getResult(player1_pick[i], player2_pick[i]));
        }
        // push the result to database
        await multiplayerRoom.update({ result: result.toString() }, { where: { room_id: req.body.room_id } });
        res.status(200).json({ player1_pick, player2_pick, result_p1_vs_p2: result });
      }
    } catch (err) {
      res.json({ status: 'failed to check the result' });
    }
  },
};