const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const {SuperAdmin} = require('../models')
const bcrypt = require('bcrypt')

async function authenticateUser(username, password, done){
    try {
        const user = await SuperAdmin.findOne({where : {username}})
        if(!user) return done(null,false, {message: "user not found"})
        const validPassword = bcrypt.compareSync(password, user.password)
        if(validPassword) return done(null, user)
        else return done(null, false,  {message: "password incorrect"})
    } catch (error) {
        return done(null, false)
    }
}

passport.use(
    new LocalStrategy({usernameField: 'username', passwordField: 'password'}, authenticateUser)
)

passport.serializeUser(
    (user, done) => done(null, user.admin_id)
)

passport.deserializeUser(
    async (admin_id, done) => done(null, await SuperAdmin.findByPk(admin_id))
)

module.exports = passport