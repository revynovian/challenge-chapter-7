'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    // parent-table user_game
    await queryInterface.createTable('user_game', {
      user_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      username: {
        type: Sequelize.STRING,
      },
      password: {
        type: Sequelize.STRING,
      },
      rank: {
        type: Sequelize.STRING,
        defaultValue: 'Bronze',
      },
      game_point: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('user_game');
  },
};
