'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    
    await queryInterface.createTable('multiplayer_room', {
      room_id: {
        primaryKey: true,
        type: Sequelize.UUID,
        allowNull: false,
      },
      player1_id: {
        type: Sequelize.UUID,
      },
      player2_id: {
        type: Sequelize.UUID,
      },
      player1_pick: {
        type: Sequelize.ARRAY(Sequelize.STRING)
      },
      player2_pick: {
        type: Sequelize.ARRAY(Sequelize.STRING)
      },
      result :{
        type: Sequelize.STRING
      }
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('multiplayer_room');
  },

};
